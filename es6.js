

console.log(1);

let humans = ["Male","Female","Brother","Sister"];
let mapHumans = humans.map(function(item){
   return item;
});
console.log(mapHumans);
// out put : ["Male", "Female", "Brother", "Sister"]


let map_humans = humans.map( (name) => name) ;
console.log(map_humans);
// out put : ["Male", "Female", "Brother", "Sister"]

let panda = {
   name:'vitou',
   age:26,
   weight:70
};
console.log(panda);
// output: {name: "vitou", age: 26, weight: 70}

let {name,age, weight} = panda;

console.log(name)
console.log(age)
console.log(weight)
//vitou
//26
//70

let {weight:width} = panda;
console.log(width);
//70

const cars = ['audi', 'nissan','rangover'];

let [car1,car2] = cars;

console.log(car1);
//audi
